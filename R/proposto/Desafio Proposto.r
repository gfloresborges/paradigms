### ATENÇÃO ###

#Por favor, executar o código no software RStudio
#Tivemos problemas de performance com a plataforma CoCalc e ela não suporta alguns comandos
#Software disponível gratuitamente em https://rstudio.com/

#instalar pacotes auxiliares
#necessário para a função 'summarise'
install.packages('dplyr')

#carregar pacote auxiliar 'dplyr'
library(dplyr)

#carregar o dataset
#atenção para o caminho do arquivo, que pode ser diferente rodando localmente
#O RStudio tem funcionalidade de importar dataset
#Caso utilize pela ferramenta, esse comando será gerado e executado
pokemons <- read.csv("Pokemon.csv", encoding="UTF-8")

#primeiro tópico
#exibe o resultado de "Quantidade por Tipo (baseada no tipo 1)"
summarise(group_by(pokemons, Type.1), Total = n())

#segundo tópico
#exibe o resultado de "Quantidade entre Lendários e não Lendários"
table(pokemons$Legendary)

#exibe o mesmo resultado em gráfico
barplot(table(pokemons$Legendary),
        main = 'É Pokémon Lendário?',
        ylab = 'Quantidade de Pokémon',
        col = c('blue', 'orange') )

#terceiro tópico
#exibe o resultado de "Quantidade por Total dos atributos de cada Pokémon com escala em intervalos de 100 unidades (0-100,101-200...)"
hist(pokemons$Total,
     main = 'Distribuição de Pokémon por potência dos atributos',
     xlab = 'Total dos atributos(potência)',
     ylab = 'Quantidade de Pokémon',
     breaks = 7,
     col = 'red')

#quarto tópico
#exibe o resultado de "Quantidade por geração"
summarise(group_by(pokemons, Generation), Total = n())

#exibe o mesmo resultado em gráfico
barplot(table(pokemons$Generation),
        main = 'Distribuição de Pokémon por geração',
        xlab = 'Geração',
        ylab = 'Quantidade de Pokémon',
        col = 'green')

#comando para desalocar o pacote e liberar memória
detach("package:dplyr", unload = TRUE)

#comando para desalocar todos os objetos e liberar memória
rm(list = ls())