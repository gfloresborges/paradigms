### ATENÇÃO ###

#Por favor, executar o código no software RStudio
#Tivemos problemas de performance com a plataforma CoCalc e ela não suporta alguns comandos
#Software disponível gratuitamente em https://rstudio.com/

#instalar pacotes auxiliares
#necessário para as funções 'summarise', 'filter', 'str_extract' e 'hash'
install.packages('dplyr')
install.packages('stringr')
install.packages('hash')

#carregar pacotes auxiliares
library(dplyr)
library(stringr)
library(hash)

#carregar o dataset
#atenção para o caminho do arquivo, que pode ser diferente rodando localmente
#O RStudio tem funcionalidade de importar dataset
#Caso utilize pela ferramenta, esse comando será gerado e executado automaticamente
lotr_data <- read.csv("lotr_data.csv", encoding="UTF-8")

#primeiro tópico
#normaliza os gêneros
lotr_data$gender[lotr_data$gender %in% c("Males", "male", "Most likely male")]<- "Male"
lotr_data$gender[lotr_data$gender == ""] <- "Undefined"

#normaliza as raças
lotr_data$race[lotr_data$race == ""] <- "Undefined"
lotr_data$race[lotr_data$race == "Hobbit"] <- "Hobbits"
lotr_data$race[lotr_data$race == "Dwarven"] <- "Dwarves"

#filtrar registros válidos
lotr_valid <- filter( lotr_data, gender != 'Undefined' & race != 'Undefined' )

#agrupar por raça e gênero
#exibe o resultado de "Como é a demografia total (quantidades por raças e sexos)?"
View(summarise(group_by(lotr_valid, race, gender), total = n()))

#segundo tópico
#adicionar coluna 'era' que corresponde a era que o habitante viveu
lotr_data$era <- 'Undefined'

#preencher a coluna era com a respectiva era de cada habitante
lotr_data$era[lotr_data$death %in% c(
	grep('first age', lotr_data$death, ignore.case = TRUE, value = TRUE),
	grep('FA', lotr_data$death, ignore.case = FALSE, value = TRUE))] <- "FA"
lotr_data$era[lotr_data$death %in% c(
	grep('second age', lotr_data$death, ignore.case = TRUE, value = TRUE),
	grep('SA', lotr_data$death, ignore.case = FALSE, value = TRUE))] <- "SA"
lotr_data$era[lotr_data$death %in% c(
	grep('third age', lotr_data$death, ignore.case = TRUE, value = TRUE),
	grep('TA', lotr_data$death, ignore.case = FALSE, value = TRUE))] <- "TA"
lotr_data$era[lotr_data$death %in% c(
	grep('fourth age', lotr_data$death, ignore.case = TRUE, value = TRUE),
	grep('FO', lotr_data$death, ignore.case = FALSE, value = TRUE))] <- "FO"

#filtrar registros válidos
lotr_valid <- filter( lotr_data, gender != 'Undefined' & race != 'Undefined' & era != 'Undefined' )

#visão geral da demografia segregada por 'era'
View(summarise(group_by(lotr_valid, era, race, gender), total = n()))

#filtra a demografia da raça 'Men'
lotr_mens_male <- filter( lotr_valid, gender == 'Male' & race == 'Men' )
lotr_mens_female <- filter( lotr_valid, gender == 'Female' & race == 'Men' )

#agrupa a demografia da raça 'Men' por 'era'
demografia_male <- table(factor(lotr_mens_male$era, levels = c('FA', 'SA', 'TA', 'FO')))
demografia_female <- table(factor(lotr_mens_female$era, levels = c('FA', 'SA', 'TA', 'FO')))

#exibe o resultado de "Qual a demografia evoluiu ao longo do tempo?"
plot(demografia_male, type='l', col = 'blue', 
		main = 'Evolução da demografia da raça \'Men\'',
        xlab = 'Eras',
        ylab = 'Quantidade de Humanos')		
lines(demografia_female, type='l', col = 'yellow')
legend(1,150, legend = c('Sexo Masculino', 'Sexo Feminino'), col=c('blue', 'yellow'), lty = 1, cex = 1.2)		

#terceiro tópico
#adicionar coluna 'age' que corresponde a idade do habitante
lotr_data$age <- 0

#filtrar registros válidos
lotr_valid <- filter( lotr_data, race != 'Undefined' &
	birth %in% c(
	grep('FA \\d+|SA \\d+|TA \\d+|FO \\d+', lotr_data$birth, ignore.case = FALSE, value = TRUE))
	& death %in% c(
	grep('FA \\d+|SA \\d+|TA \\d+|FO \\d+', lotr_data$death, ignore.case = FALSE, value = TRUE)) )

#calcula idade extra referente a era 'First Age' que durou 590 anos
calcula_idade_extra_FA <- function(ano_nascimento) {
	return (590 - ano_nascimento)
}

#calcula idade extra referente a era 'Second Age' que durou 3441 anos
calcula_idade_extra_SA <- function(ano_nascimento) {
	return (3441 - ano_nascimento)
}

#calcula idade extra referente a era 'Third Age' que durou 3021 anos
calcula_idade_extra_TA <- function(ano_nascimento) {
	return (3021 - ano_nascimento)
}

#calcula idade baseado em uma data de nascimento e data de morte
calcula_idade <- function (data_nascimento, data_morte) {
	#separa as datas em era e ano, pois recebe no padrão 'FA 340'
	era_nascimento <- unlist(strsplit(data_nascimento, ' '))[1]
	ano_nascimento <- as.numeric(unlist(strsplit(data_nascimento, ' '))[2])
	era_morte <- unlist(strsplit(data_morte, ' '))[1]
	ano_morte <- as.numeric(unlist(strsplit(data_morte, ' '))[2])
		
	if( era_nascimento == era_morte ) {
		return(ano_morte - ano_nascimento)
	} else {
		#caso as eras de nascimento e morte sejam diferentes, calcula a idade baseada nas respectivas eras
		if( era_nascimento == 'FA' ) {
			if( era_morte == 'SA' ) return(calcula_idade_extra_FA(ano_nascimento)+ano_morte)
			if( era_morte == 'TA' ) return(calcula_idade_extra_FA(ano_nascimento)+calcula_idade_extra_SA(0)+ano_morte)
			if( era_morte == 'FO' ) return(calcula_idade_extra_FA(ano_nascimento)+calcula_idade_extra_SA(0)+calcula_idade_extra_TA(0)+ano_morte)
		}
		if( era_nascimento == 'SA' ) {
			if( era_morte == 'TA' ) return(calcula_idade_extra_SA(ano_nascimento)+ano_morte)
			if( era_morte == 'FO' ) return(calcula_idade_extra_SA(ano_nascimento)+calcula_idade_extra_TA(0)+ano_morte)
		}
		if( era_nascimento == 'TA' ) {
			return(calcula_idade_extra_TA(ano_nascimento)+ano_morte)
		}
		#só irá executar essa linha caso os parametros recebidos sejam inválidos
		return (0)
	}
}

#loop calculando e atribuindo a idade de cada habitante
for( row in 1:nrow(lotr_valid) ) {
	data_nascimento <- str_extract(lotr_valid[row, "birth"], 'FA \\d+|SA \\d+|TA \\d+|FO \\d+')
	data_morte <- str_extract(lotr_valid[row, "death"], 'FA \\d+|SA \\d+|TA \\d+|FO \\d+')
	
	lotr_valid[row, "age"] <- calcula_idade(data_nascimento, data_morte)
}

#calcula a idade média dos habitantes de cada raça
media_idade_por_race <- aggregate(lotr_valid$age, list(lotr_valid$race), FUN = mean)

#arredonda a coluna da média em duas casas decimais
media_idade_por_race[,'x'] <- round(media_idade_por_race[,'x'], 2)

#exibe o resultado de "Qual a idade média dos habitantes de cada raça?"
setNames(media_idade_por_race, c('raça', 'média'))

#quarto tópico
#calcula a idade média dos habitantes de cada raça separado em cada era
media_idade_por_race_e_era <- aggregate(lotr_valid$age, list(lotr_valid$era, lotr_valid$race), FUN = mean)

#arredonda a coluna da média em duas casas decimais
media_idade_por_race_e_era[,'x'] <- round(media_idade_por_race_e_era[,'x'], 2)

#Como a idade média dos habitantes de cada raça evoluiu ao longo do tempo?
setNames(media_idade_por_race_e_era, c('era', 'raça', 'média'))

#quinto tópico
total_habitantes = nrow(lotr_data)

#normaliza os solteiros
lotr_data$spouse[lotr_data$spouse %in% c( '', 
	grep('none', lotr_data$spouse, ignore.case = TRUE, value = TRUE), 
	grep('unknown', lotr_data$spouse, ignore.case = TRUE, value = TRUE),
	grep('never', lotr_data$spouse, ignore.case = TRUE, value = TRUE))]<- 'Not married'

#filtra os habitantes casados
casados <- filter( lotr_data, spouse != 'Not married' )

#calcula a taxa de casamentos
taxa_casamentos <- round(nrow(casados) / total_habitantes * 100, digits = 2)

#exibe o resultado de "Qual a taxa de casamentos?"
paste('Taxa de casamentos:', taxa_casamentos, '%')

#sexto tópico
#exibe o resultado de "Como os casamentos variam de raça para raça?"
barplot(table(casados$race),
	main = 'Casamentos por raça',
	xlab = 'Raças',
	ylab = 'Quantidade de casados')

#sétimo tópico
casamentos_interraciais <- 0

#loop entre os habitantes casados
for( row in 1:nrow(casados) ) {
	raca_habitante <- casados[row, "race"]
	nome_spouse <- casados[row, "spouse"]
	
	#caso o nome do cônjuje do habitante exista na tabela de casados...
	if( nome_spouse %in% casados$name ) {
		#então é verificado se o casamento é inter-racial
		if( raca_habitante != casados[casados$name == nome_spouse, "race"] ) {
			#caso verdadeiro, incrementa o contador
			casamentos_interraciais <- casamentos_interraciais + 1
		}	
	}	
}

#calcula o total de casamentos dividindo a quantidade total de casados por 2
total_casamentos <- round(nrow(casados) / 2)

#calcula a frequência relativa dos casamentos inter-raciais
freq_relativa = round( casamentos_interraciais / total_casamentos * 100, digits = 2 )

#exibe o resultado de "Qual a frequência de casamentos inter-raciais?"
paste('Frequência Absoluta de casamentos inter-raciais:', casamentos_interraciais)
paste('Frequência Relativa de casamentos inter-raciais:', freq_relativa, '%')

#oitavo tópico
#normaliza a coluna hair
lotr_data$hair <- tolower(lotr_data$hair)
lotr_data$hair[lotr_data$hair %in% c(" ", "", "None", "none" )]<- "Undefined"
lotr_data$hair[grepl('auburn',lotr_data$hair)]<-"Auburn"
lotr_data$hair[grepl('black', lotr_data$hair)]<-"Black"
lotr_data$hair[grepl('blond', lotr_data$hair)]<- "Blond"
lotr_data$hair[grepl('brown', lotr_data$hair)]<-"Brown"
lotr_data$hair[grepl('dark', lotr_data$hair)]<-"Dark"
lotr_data$hair[grepl('golden', lotr_data$hair)]<- "Golden"
lotr_data$hair[grepl('grey', lotr_data$hair)]<- "Grey"
lotr_data$hair[grepl('white',lotr_data$hair)]<-"White"
lotr_data$hair[grepl('silver',lotr_data$hair)]<-"Silver"

#normaliza a coluna height
lotr_data$height[lotr_data$height == ''] <- 'Undefined'

#filtrar registros válidos
lotr_valid <- filter( lotr_data,race != 'Undefined')

#fazer lista das raças existentes
df_race<-as.data.frame(table(lotr_valid$race))

#extrair os dados para variáveis separadas
possiveis_alturas = as.character(as.data.frame(table(lotr_valid$height))$Var1)
possiveis_cabelos = as.character(as.data.frame(table(lotr_valid$hair))$Var1)
possiveis_racas = as.character(as.data.frame(table(lotr_valid$race))$Var1)

#garante a integridade do dado
is.integer0 <- function(x) {
  is.integer(x) && length(x) == 0L
}

for (j in 1:1){
    nova_linha <- hash()
    for(i in 1:length(possiveis_racas)) {
        df_raca_espc = lotr_valid[lotr_valid$race==possiveis_racas[i],]
        frequencia_cabelos = as.data.frame(table(df_raca_espc$hair))
        total_cabeloJ_na_racaI = frequencia_cabelos[frequencia_cabelos$Var1==possiveis_cabelos[j],][,2]
        if(is.integer0(total_cabeloJ_na_racaI)){
            nova_linha[[possiveis_racas[i]]]=0
        }
        else{
            nova_linha[[possiveis_racas[i]]]= total_cabeloJ_na_racaI
        }
    }
    b<-values(nova_linha)
    df<-as.data.frame(b)
    names(df)[names(df)=='b']<- possiveis_cabelos[j]
}

for (j in 2:length(possiveis_cabelos)){
    nova_linha <- hash()
    for(i in 1:length(possiveis_racas)) {
        df_raca_espc = lotr_valid[lotr_valid$race==possiveis_racas[i],]
        frequencia_cabelos = as.data.frame(table(df_raca_espc$hair))
        total_cabeloJ_na_racaI = frequencia_cabelos[frequencia_cabelos$Var1==possiveis_cabelos[j],][,2]
        if(is.integer0(total_cabeloJ_na_racaI)){
            nova_linha[[possiveis_racas[i]]]=0
        }
        else{
            nova_linha[[possiveis_racas[i]]]= total_cabeloJ_na_racaI
        }
    }
    b<-values(nova_linha)
    L<-as.data.frame(b)
    df = cbind(df,L)
    names(df)[names(df)=='b']<- possiveis_cabelos[j]
}

#exibe o resultado de "Como traços (cabelo, altura) físicos se relacionam com as raças?"
#Relação entre cabelo e raça
View(df)

for (j in 1:1){
    nova_linha <- hash()
    for(i in 1:length(possiveis_racas)) {
        df_raca_espc = lotr_valid[lotr_valid$race==possiveis_racas[i],]
        frequencia_alturas = as.data.frame(table(df_raca_espc$height))
        total_alturaJ_na_racaI = frequencia_alturas[frequencia_alturas$Var1==possiveis_alturas[j],][,2]
        if(is.integer0(total_alturaJ_na_racaI)){
            nova_linha[[possiveis_racas[i]]]=0
        }
        else{
            nova_linha[[possiveis_racas[i]]]= total_alturaJ_na_racaI
        }
    }
    b<-values(nova_linha)
    df<-as.data.frame(b)
    names(df)[names(df)=='b']<- possiveis_alturas[j]
}

for (j in 2:length(possiveis_alturas)){
    nova_linha <- hash()
    for(i in 1:length(possiveis_racas)) {
        df_raca_espc = lotr_valid[lotr_valid$race==possiveis_racas[i],]
        frequencia_alturas = as.data.frame(table(df_raca_espc$height))
        total_alturaJ_na_racaI = frequencia_alturas[frequencia_alturas$Var1==possiveis_alturas[j],][,2]
        if(is.integer0(total_alturaJ_na_racaI)){
            nova_linha[[possiveis_racas[i]]]=0
        }
        else{
            nova_linha[[possiveis_racas[i]]]= total_alturaJ_na_racaI
        }
    }
    b<-values(nova_linha)
    L<-as.data.frame(b)
    df = cbind(df,L)
    names(df)[names(df)=='b']<- possiveis_alturas[j]
}

#exibe o resultado de "Como traços (cabelo, altura) físicos se relacionam com as raças?"
#Relação entre altura e raça
View(df)

#desalocar os pacotes e liberar memória
detach("package:dplyr", unload = TRUE)
detach("package:stringr", unload = TRUE)
detach("package:hash", unload = TRUE)

#comando para desalocar todos os objetos e liberar memória
rm(list = ls())