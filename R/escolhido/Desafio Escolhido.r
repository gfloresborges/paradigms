### ATENÇÃO ###

#Por favor, executar o código no software RStudio
#Tivemos problemas de performance com a plataforma CoCalc e ela não suporta alguns comandos
#Software disponível gratuitamente em https://rstudio.com/

#carregar o dataset
#atenção para o caminho do arquivo, que pode ser diferente rodando localmente
#O RStudio tem funcionalidade de importar dataset
#Caso utilize pela ferramenta, esse comando será gerado e executado
df_original <- read.csv("starwars.csv", encoding="UTF-8")

#primeiro tópico
#filtra os registros onde a coluna especie é diferente de vazia
df_original_sem_NA_nas_especies <- df_original[!is.na(df_original$species), ]

#filtra apenas os humanos
df_humanos <- df_original_sem_NA_nas_especies[df_original_sem_NA_nas_especies$species == 'Human', ]

#exibe o resultado de "Quantos HUMANOS estão presentes no livro?"
cat('O números de HUMANOS no livro é:',nrow(df_humanos))

#segundo tópico
#Exclui os NA da coluna de genero e salva os dados em novo dataframe
df_original_sem_NA_no_genero<-df_humanos[!is.na(df_humanos$gender), ]

#Cria um novo dataframe somente com genero feminino e conta quantos humanos feminos tem
df_genero_fem <- df_original_sem_NA_no_genero[df_original_sem_NA_no_genero$gender == 'female', ]

#Cria um novo dataframe somente com genero masculino e conta quantos humanos masculino tem
df_genero_masc <- df_original_sem_NA_no_genero[df_original_sem_NA_no_genero$gender == 'male', ]

#exibe o resultado de "Quantos HUMANOS são MASCULINOS e quantos são FEMININOS?"
cat('A quantidades de HUMANOS que são FEMININOS é:', nrow(df_genero_fem))
cat('A quantidades de HUMANOS são MASCULINO é:',nrow(df_genero_masc))

#terceiro tópico
#Exclui da coluna birth_year os NA
df_birth_sem_NA <- df_humanos[!is.na(df_humanos$birth_year), ]

#Originalmente a coluna birth_year está em Factor, então é necessário transformar ela em numérico para calcular a média
df_novo_birth <-transform(df_birth_sem_NA, birth_year = as.numeric(paste(birth_year)))

#calcula a média do ano de nascimento
media_birth_year<-mean(x = df_novo_birth$birth_year)

#exibe o resultado de "Qual o ano de nascimento médio dos HUMANOS em BBY?"
cat('O ano médio de nascimento dos HUMANOS em BBY é:',  media_birth_year)

#quarto tópico
#exibe o resultado de "Qual a ALTURA do R2-D2 em cm?"
cat('A altura em cm do R2-D2 é:',df_original[df_original$name == 'R2-D2', "height"])

#quinto tópico
#filtra os jedi do livro
df_jedi_is_true <- df_original[df_original$jedi == TRUE, ]

#exibe o resultado de "Quantos JEDI estão presentes no livro?"
cat('O número de JEDI no livro no livro é:',nrow(df_jedi_is_true))

#sexto tópico
#evita a exibição da coluna jedi
df_jedi_is_true$jedi <- NULL

#exibe o resultado de "Mostre todos os JEDI do livro (mostrar todas as categorias menos a 'jedi': name,height,mass,...,species)?"
head(df_jedi_is_true,nrow(df_jedi_is_true))

#sétimo tópico
#altera o nome dos jedi para Irineu
df_jedi_is_true$name <- 'Irineu'

#exibe o resultado de "Mude o nome de todos os JEDI para 'Irineu' e mostre novamente os JEDI?"
head(df_jedi_is_true,nrow(df_jedi_is_true))

#comando para desalocar todos os objetos e liberar memória
rm(list = ls())