--função que conta o total de digitos de um número
countDigits number = length (show number)

--função que retorna uma lista com os dígitos de um número
digitsToList = map (read . return) . show

--função que calcula a potência
pow expo number = number ^ expo

--função que calcula a potência dos dígitos de um número onde o expoente será sempre o total de dígitos
powDigits number = map (pow (countDigits number)) (digitsToList number)

--função que valida se um número é Armstrong
isArmstrong number = sum (powDigits number) == number

--função que retorna se um número é ou não Armstrong
checkArmstrong number = if isArmstrong number then show number ++ " is an Armstrong number" else show number ++ " isn't an Armstrong number"

--exemplo com número que não é Armstrong
checkArmstrong 152

--exemplo com número que é Armstrong
checkArmstrong 153

--função que verifica se a subtração é válida
subIsValid number sub = number - sub >= 0

--função que converte um número para algarismos romanos
convert :: Integer -> String -> String
convert number roman =
    if number == 0
        then roman
        else if subIsValid number 1000
                then convert (number-1000) (roman++"M")
                else if subIsValid number 900
                        then convert (number-900) (roman++"CM")
						else if subIsValid number 500
								then convert (number-500) (roman++"D")
								else if subIsValid number 400
										then convert (number-400) (roman++"CD")
										else if subIsValid number 100
												then convert (number-100) (roman++"C")
												else if subIsValid number 90
														then convert (number-90) (roman++"XC")
														else if subIsValid number 50
																then convert (number-50) (roman++"L")
																else if subIsValid number 40
																		then convert (number-40) (roman++"XL")
																		else if subIsValid number 10
																				then convert (number-10) (roman++"X")
																				else if subIsValid number 9
																						then convert (number-9) (roman++"IX")
																						else if subIsValid number 5
																								then convert (number-5) (roman++"V")
																								else if subIsValid number 4
																										then convert (number-4) (roman++"IV")
																										else convert (number-1) (roman++"I")

--função que exibe a conversão de um número para algarismos romanos
convertToRoman number = convert number (show number ++ " equals ")

--exemplos de conversão
convertToRoman 2019
convertToRoman 2020
convertToRoman 2021