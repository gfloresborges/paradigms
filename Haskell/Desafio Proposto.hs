--função que verifica se um número pertence a sequência Fibonacci
isFiboDNA number prev total =
	if total == number
		then show number ++ " is a perfect DNA!"
		else if total > number
			then show number ++ " isn't a perfect DNA!"
			else isFiboDNA number total (prev+total)

--função que verifica se um DNA é perfeito
checkDNA dna = isFiboDNA dna 0 1

--exemplo com um DNA que não é perfeito
checkDNA 63245987

--exemplo com um DNA que é perfeito utilizando o 39º número Fibonacci
checkDNA 63245986