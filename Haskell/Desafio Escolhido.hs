import System.Random(randomRIO)

--gera um numero randomico do local onde os amigos estão
localAmigos <- randomRIO (1,11::Integer)

--salva locais aleatorios como amaldiçoados
localAmalUm <- randomRIO (1,11::Integer)
localAmalDois <- randomRIO (1,11::Integer)
localAmalTres <- randomRIO (1,11::Integer)

--armazena os locais amaldiçoados em uma lista
locaisAmaldicoados = [ (show localAmalUm), (show localAmalDois), (show localAmalTres) ]

--função que executa a rodada inteira
--OBS: o comando getLine apresentou intermitencia na plataforma CoCalc online e as vezes trava a execução
--nestes casos é necessário interromper o kernel e executar o programa novamente
proximaRodada :: String -> [String] -> Integer -> IO ()
proximaRodada posAmigos locaisAmal vidas = if vidas == 0
												then putStrLn "Suas chances se esgotaram, pois você acessou dois locais amaldiçoados!"
												else do
													putStrLn "Encontre seus amigos, digite um numero entre 1 e 11:"
													posAtual <- getLine
													if posAtual == posAmigos
														then putStrLn "Parabéns, Você encontrou  seus amigos!"
														else if posAtual `elem` locaisAmal
																then do
																	putStrLn "Lugar Amaldiçoado! Tente Novamente"
																	proximaRodada posAmigos locaisAmal (vidas-1)
																else do
																	putStrLn "Você não encontrou seus amigos ainda. Tente novamente!"
																	proximaRodada posAmigos locaisAmal vidas


proximaRodada (show localAmigos) locaisAmaldicoados 2