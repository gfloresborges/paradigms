#solução do desafio
import random
from threading import Timer

#define se o tempo já esgotou
gameOver = False

#tipo de dados tuple para controlar as direções
dirs = (0,0,0,0)

'''locais do Jedi's Housing
tipo de dados utilizado: dict (coleção do tipo dicionário com propriedades e valores)
ver tutorial: https://docs.python.org/3/library/stdtypes.html#typesmapping'''

entrance = {'name': 'Entrada', 'directions': dirs, 'msg': ''}
livingroom = {'name': 'Sala de Estar', 'directions': dirs, 'msg': ''}
kitchen = {'name': 'Cozinha', 'directions': dirs, 'msg':''}
jedistudyroom = {'name': 'Sala de Estudos', 'directions': dirs, 'msg': ''}
jediboysroom = {'name': 'Dormitório dos meninos Jedi', 'directions': dirs, 'msg': ''}
jedigirlsroom = {'name': 'Dormitório das meninas Jedi', 'directions': dirs, 'msg': ''}

#definição das direções de cada local (N,L,S,O) conforme o mapa
entrance['directions'] = (0,livingroom,0,0)
livingroom['directions'] = (jedistudyroom,jedigirlsroom,kitchen,entrance)
kitchen['directions'] = (livingroom,0,0,0)
jedistudyroom['directions'] = (0,jediboysroom,livingroom,0)
jediboysroom['directions'] = (0,0,jedigirlsroom,jedistudyroom)
jedigirlsroom['directions'] = (jediboysroom,0,0,livingroom)

#definição de onde o sabre de luz pode estar
# tipo de dados utilizado: list
#https://docs.python.org/3/tutorial/introduction.html#lists
rooms = [livingroom,kitchen,jedistudyroom,jediboysroom,jedigirlsroom]

#função que retorna o próximo local
def getNextRoom(currentRoom, move):
    possibleDirs = currentRoom.get("directions")
    if move == 'N':
        return possibleDirs[0]
    elif move == 'L':
        return possibleDirs[1]
    elif move == 'S':
        return possibleDirs[2]
    elif move == 'O':
        return possibleDirs[3]
    else:
      return 0

#verifica se a jogada é válida
def isValidMove(room, move):
    nextRoom = getNextRoom(room, move)
    if nextRoom == 0:
        print('\n[INFO]  A direção que você escolheu não tem saída! Tente Novamente.')
        return False
    return True

#verifica se o input é válido
def isValidInput(readValue):
    if readValue not in ['L','O','N','S']:
        print('\n[ERRO]  Você digitou um caractere errado. Tente novamente.')
        return False
    return True

#define que o tempo acabou
def timeOver():
    global gameOver
    gameOver = True 
        
#função principal que executa o programa
def main():

    #aqui estamos usando o random para mudar o local do sabre em cada jogada
    room_with_lightsaber = random.choice(rooms)

    #define que o sabre ainda não foi encontrado
    lightsaber_found = False

    #define que a sala iniciar é a entrada
    room = entrance

    #Jogador terá entre 20 e 40 segundos para encontrar o sabre  
    gameMaxTime = random.randint(20, 40)

    print('Bem-vindo Padawan! Você consegue localizar onde está o seu sabre de luz?\n\nDurante o último treino, você acabou deixando seu sabre de luz em algum lugar da Jedi\'s Housing. E agora, existe a ameaça do Darth Vader estar rondando a Jedi\'s Housing.\n\nPara ajudar os seus colegas neste possível ataque dos Sith, você precisar encontrar o seu sabre de luz, antes que o Darth Vader o encontre!\n\nPara tanto, você deve percorrer os locais da Jedi\'s Housing! \nUtilize as direções: Norte (N), Leste (L), Sul (S) e Oeste (O) para entrar nos locais e localizar o seu sabre de luz.\n\nLembre-se que você está agora na entrada!')

    input('\n[AÇÃO]  Pressione Enter para começar: ')

    #inicia a marcação do tempo após o jogador decidir começar
    Timer(gameMaxTime, timeOver).start()

    #continua o jogo enquanto o sabre não foi encontrado
    while not lightsaber_found:
    #interrompe o jogo caso o tempo esteja esgotado
        if gameOver:
            break

        move = input('\n[AÇÃO]  Escolha uma direção para se mover(N, L, S ou O): ').upper()

        if isValidInput(move):
            if isValidMove(room, move):
                room = getNextRoom(room, move)
                if room == room_with_lightsaber:
                    lightsaber_found = True
                else:
                    print('\n[INFO]  O sabre de luz não está aqui!!!\n[INFO] Seu local atual: ' + room.get("name"))


    if lightsaber_found == True:
        print('\n[PARABENS]  Você encontrou o sabre de luz!!! Local: ' + room.get("name") + '\n')
    else:
        print('\n[GAME OVER]  Darth Vader encontrou você!!!O lado sombrio da força venceu!!!')

#executa a função main
main()