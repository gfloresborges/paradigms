import random
import time

class Die:
    def throw(self):
        return random.randint(1, 6)

def applySpecialAction(actionResult, currentPlace, remainingMoves, resultDie):
    splitedResult = actionResult.split(';')
    move = int(splitedResult[1])
    if splitedResult[0] == 'R':
        remainingMoves += move
    elif splitedResult[0] == 'C':
        currentPlace += move
    elif splitedResult[0] == 'D':
        currentPlace += move + 1

    return (currentPlace, remainingMoves)

def play():
    moveDescription = {
        1 : 'O macaco Twelves apareceu e roubou os dados. Fique uma rodada sem jogar.',
        2 : 'Um gorila te jogou longe. Volte uma casa.',
        3 : 'Um aglomerado de morcegos bloqueou a visão de seu adversário, aproveite e avance mais duas casas.',
        4 : 'Um forte vento virou o dado e alterou sua jogada. Ande o equivalente à sua jogada mais uma casa extra.',
        5 : 'Um jacaré mordeu seu pé. Fique duas rodadas sem jogar.',
        6 : 'Uma manada de rinocerontes veio em sua direção. Você fugiu e voltou duas casas.',
        7 : 'Estranhamente, nada aconteceu. Prossegue sua jogada.',
        8 : 'Uma enchente te deixou impossibilitado de se mover. Não jogue a próxima rodada'
    }

    actionResults = {
        1 : 'R;-1',
        2 : 'C;-1',
        3 : 'C;2',
        4 : 'D;1',
        5 : 'R;-2',
        6 : 'C;-2',
        7 : 'N;0',
        8 : 'R;-1',
    }

    actions = [ 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8 ]

    die = Die()

    print('Você comprou um tabuleiro Jumanji. E você decide testar o jogo sozinho.\n\nEm sua primeira jogada, sua casa começa a tremer, e tambores começam a rugir, em seguida o espírito do Robin Williams aparece e lhe alerta: - “O desafio começou e não poderá ser parado até seu final”. Logo após, um caçador também é invocado e ele promete transformar seu mundo no de Jumanji.\n\nAgora, você deve acabar o jogo primeiro para que seu mundo volte ao normal.')

    finalPlace = 30
    currentPlace = 0
    remainingMoves = 15

    
    #jogo continua enquanto existir rodadas restantes
    while remainingMoves > 0:
        #interrompe o jogo caso o jogador chegue no fim do tabuleiro
        if currentPlace >= finalPlace:
            break
      
        #interrompe a execução até o jogador jogar o dado
        input('\n[AÇÃO]  Pressione enter para jogar o dado: ')

        #sorteio um valor do dado
        result = die.throw()

        #avança a posição do jogador no tabuleiro
        currentPlace += result        

        #sorteio uma ação especial
        specialMove = actions[random.randint(0, len(actions) - 1)]

        #aplicar ação especial
        actionResult = actionResults[specialMove]
        currentPlace, remainingMoves = applySpecialAction(actionResult, currentPlace, remainingMoves, result)

        remainingMoves -= 1

        actions.pop(specialMove)

        #comunica resultado ao jogador
        time.sleep(1)
        print('\n[INFO]  Ande ' + str(result) + ' casas')
        time.sleep(2)

        #interrompe o jogo caso o jogador chegue no fim do tabuleiro
        if currentPlace >= finalPlace:
            break

        print('\n[INFO]  ' + moveDescription[specialMove])
        print('\n[INFO]  Você está na posição ' + str(currentPlace) + ' e ainda possui ' + str(remainingMoves) + ' jogadas restantes')
        time.sleep(2)        

        

    if currentPlace >= finalPlace:
        print('\n[PARABÉNS] Você chegou no fim do tabuleiro e seu mundo voltou ao normal!!!')
    else:
        print('\n[GAMEOVER] Suas jogadas se esgotaram!!!')      
 


#inicia o jogo
play()