# solução do seu desafio aqui
import random
import time

class Elf:
    def __init__(self, life):
        self.life = life
        self.power = 16
        self.alive = True

    def isAlive(self):
        return self.alive

    def attack(self):
        return self.power

    def takeDamage(self, damage):
        self.life -= damage
        if self.life <= 0:
            self.alive = False

class Dwarf:
    def __init__(self, life):
        self.life = life
        self.power = 12
        self.alive = True

    def isAlive(self):
        return self.alive

    def attack(self):
        return self.power

    def takeDamage(self, damage):
        self.life -= damage
        if self.life <= 0:
            self.alive = False

# Valida as entradas
def readInputAndValidateForPositive(personagem):
    value = -1
    while value < 0 :
        try:
            value = int(input("[AÇÃO]  Determine o tamanho do exército de "+ str(personagem)  +": "))
            if(value >= 0 ):
                return value
            else:
                print("\n[ERRO]  Você digitou um numero negativo.\n")
        except:
            print("\n[ERRO]  Você digitou um caractere inválido\n")



def battle():
    livingElves = [] 
    livingDwarves = []

    print('Olá, Você está prestes a presenciar a maior batalha entre Elfos e Anões que a terra média já teve!!!')

    amountElves = readInputAndValidateForPositive('Elfos')
    amountDwarves = readInputAndValidateForPositive('Anões')
    

    # preenche uma lista com elfos conforme quantidade informada pelo jogador
    for i in range(amountElves):
        newElf = Elf(random.randint(20, 40))
        livingElves.append(newElf)

    # preenche uma lista com anões conforme quantidade informada pelo jogador
    for i in range(amountDwarves):
        newDwarf = Dwarf(random.randint(30, 50))
        livingDwarves.append(newDwarf)

    # a batalha ocorre enquanto as duas raças tem personagens vivos
    while len(livingElves) > 0 and len(livingDwarves) > 0:
        #cada elfo ataca o primeiro anão da lista se ele estiver vivo
        for elf in livingElves:
            currentDwarf = livingDwarves.pop(0)
            if currentDwarf.isAlive():
                currentDwarf.takeDamage( elf.attack() )
            livingDwarves.append(currentDwarf)

        #cada anão ataca o primeiro elfo da lista se ele estiver vivo
        for dwarf in livingDwarves:
            currentElf = livingElves.pop(0)
            if currentElf.isAlive():
                currentElf.takeDamage( dwarf.attack() )
            livingElves.append(currentElf)

        #remove todos os elfos que morreram na respectiva rodada
        for elf in reversed(livingElves):
            if not elf.isAlive():
                livingElves.remove(elf)

        #remove todos os anões que morreram na respectiva rodada
        for dwarf in reversed(livingDwarves):
            if not dwarf.isAlive():
                livingDwarves.remove(dwarf)

    print('\n[INFO]  A batalha começou...')

    time.sleep(3)

    print('\n[INFO]  Os exércitos estão lutando...')

    time.sleep(4)

    #se restou elfos vivos então significa que eles venceram
    if len(livingElves) > 0:
        print('\n[INFO]  Os Elfos venceram!!!')

    #se restou anões vivos então significa que eles venceram
    elif len(livingDwarves) > 0:
        print('\n[INFO]  Os Anões venceram!!!')

    #else significa que não restou ninguém vivo
    else:
        print('\n[INFO]  Não existe raça vencedora pois não restou ninguém vivo :(')

#inicia a batalha
battle()